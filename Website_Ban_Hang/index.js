import {
  btnPurchase,
  getPhoneList,
  phoneList,
  renderShopCart,
  searchIndex,
  searchPhone,
  tinhTong,
} from "./controller/phone.controller.js";

const userShopCart_LOCALSTORAGE = "userShopCart_LOCALSTORAGE";

var shopCart = [];
var userShopCartJson = localStorage.getItem(userShopCart_LOCALSTORAGE);
if (userShopCartJson != null) {
  shopCart = JSON.parse(userShopCartJson);

  renderShopCart(shopCart);
  tinhTong(shopCart);
}

getPhoneList();
btnPurchase();

let addToCart = (phoneID) => {
  let index = searchIndex(shopCart, phoneID);
  let index2 = searchIndex(phoneList, phoneID);
  // console.log(index, index2);

  if (index == -1) {
    let cartItem = { ...phoneList[index2], soLuong: 1 };
    // console.log("cartItem: ", cartItem);
    shopCart.push(cartItem);
    // console.log("shopCart: ", shopCart);
    renderShopCart(shopCart);
  } else {
    shopCart[index].soLuong++;
    renderShopCart(shopCart);
  }
  tinhTong(shopCart);
  localStorage.setItem(userShopCart_LOCALSTORAGE, JSON.stringify(shopCart));
  btnPurchase();
};

window.getPhoneList = getPhoneList;
window.addToCart = addToCart;

