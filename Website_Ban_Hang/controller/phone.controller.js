export let renderPhoneList = (phoneList) => {
  var contentHTML = "";

  phoneList.forEach((phone) => {
    contentHTML += `<div class="card border-primary mx-1" >
  <div class="card-header bg-dark text-center text-white">${phone.name}</div>
  <img class="card-img-top pt-2" src=${phone.img}></img>
  <div class="card-body text-primary">
    <h5 class="card-title btn btn-primary">$${phone.price}</h5>
    <p class="card-text">${phone.desc}</p>
    <hr/>
    <div class='d-flex justify-content-center'>
        <button onclick=addToCart("${phone.id}") class="btn btn-success px-5">Add</button>
    </div>
  </div>
</div>`;
  });
  document.getElementById("phoneItems").innerHTML = contentHTML;
};

export let batLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
export let tatLoading = () => {
  document.getElementById("loading").style.display = "none";
};

const BASE_URL = "https://630eebe037925634188387dd.mockapi.io";
export let phoneList;
export let getPhoneList = () => {
  batLoading();
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      renderPhoneList(res.data);
      phoneList = res.data;
      //  console.log("phoneList: ", phoneList);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
};

export let searchIndex = (itemList, idSearch) => {
  let index = itemList.findIndex((item) => {
    return item.id == idSearch;
  });
  return index;
};

export let renderShopCart = (cartList) => {
  let contentTbody = "";
  cartList.forEach((item) => {
    contentTbody += ` <tr>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td class="w-10"><img src=${item.img} class="cartItemImg"/></td>
        <td> <button onclick=changeAmount("${item.id}",false) class="btn btn-warning">-</button>
          <span class="mx-2">${item.soLuong}</span>
          <button
            onclick=changeAmount("${item.id}",true)
            class="btn btn-success"
          >+</button>
        </td>
        <td>${item.price * item.soLuong}</td>
        <td>
          <button
            onclick=removeFromCart("${item.id}")
            class="btn btn-danger"
          >Remove</button>
        </td>
      </tr> `;
  });

  document.getElementById("cartTbody").innerHTML = contentTbody;
};


export let tinhTong = (cartList) => {
  document.getElementById("totalCharge").innerHTML = cartList.reduce(
    (total, currentItem) => {
      return total + currentItem.soLuong * currentItem.price;
    },
    0
  );
}