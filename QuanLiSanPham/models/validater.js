var validation = {
    kiemTraRong: function (value, idError, message) {
        if (value.length == 0) {
            document.getElementById(idError).innerText = message;
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    kiemTraDoDai: function (value, idError, message, min, max) {
        if (value.length < min || value.length > max) {
            document.getElementById(idError).innerText = message;
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    kiemTraGia: function (value, idError, message, min, max) {
        if (value < min || value > max) {
            document.getElementById(idError).innerText = message;
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
};